function A = randomness(limit,n,m)
A = rand(limit,n,m);
A = arrayfun(@floor,A);
end